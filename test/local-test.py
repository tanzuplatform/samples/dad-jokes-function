# Python program to convert text
# file to JSON

import random 

filename = 'apps/dad-jokes-function/jokes.txt'

jokes = []
with open(filename) as fh:
    for line in fh:
        command, description = line.strip().split('<>', 1)
        jokes.append([command.strip(), description.strip()])

joke = random.choice (jokes)

print(f'<h1>Dad jokes! <img href="https://ca.slack-edge.com/E8YCM8N9L-W013Y7J1CKE-981ec80fa00f-72"/></h1><h2>{joke[0]}</h2><h2>{joke[1]}</h2>')
 