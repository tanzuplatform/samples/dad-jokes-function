from typing import Any
import random

def main(req: Any):
    jokes = []
    with open('jokes.txt') as fh:
        for line in fh:
            command, description = line.strip().split('<>', 1)
            jokes.append([command.strip(), description.strip()])

    joke = random.choice (jokes)

    return(f"""<body>
    <h1>Dad jokes! <img src="https://ca.slack-edge.com/E8YCM8N9L-W013Y7J1CKE-981ec80fa00f-72"/></h1>
        <br/>
        <h2>{joke[0]}</h2>
        <br/>
        <h2>{joke[1]}</h2>
    </body>
    """)
    